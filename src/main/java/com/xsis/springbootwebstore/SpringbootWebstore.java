package com.xsis.springbootwebstore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;

import com.xsis.springbootwebstore.model.Product;
import com.xsis.springbootwebstore.model.Customer;
import com.xsis.springbootwebstore.model.Users;

import com.xsis.springbootwebstore.repository.ProductRepository;
import com.xsis.springbootwebstore.repository.CustomerRepository;
import com.xsis.springbootwebstore.repository.UserRepository;

import java.math.BigDecimal;

@SpringBootApplication
public class SpringbootWebstore {

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private UserRepository userRepository;

	public static void main(String[] args) {
		SpringApplication.run(SpringbootWebstore.class, args);
	}

	@Bean
	CommandLineRunner runner() {
		return args -> {

			// username: user password: user
		//	userRepository.save(new Users("user", "$2a$04$1.YhMIgNX/8TkCKGFUONWO1waedKhQ5KrnB30fl0Q01QKqmzLf.Zi", "USER"));
			// username: admin password: admin
		//	userRepository
		//			.save(new Users("admin", "$2a$04$KNLUwOWHVQZVpXyMBNc7JOzbLiBjb9Tk9bP7KNcPI12ICuvzXQQKG", "ADMIN"));

			/* Customer customer1 = new Customer("Bang", "Jakarta");
			Customer customer2 = new Customer("Kang", "Bandung");
			Customer customer3 = new Customer("Mas", "Semarang");
			Customer customer4 = new Customer("Bli", "Bali");
			Customer customer5 = new Customer("Bang", "Medan");

			customerRepository.save(customer1);
			customerRepository.save(customer2);
			customerRepository.save(customer3);
			customerRepository.save(customer4);
			customerRepository.save(customer5); */

			

			/* Product oppo = new Product("Oppo", new BigDecimal(5000));
			oppo.setCategory("SmartPhone");

			Product samsung = new Product("Samsung S5", new BigDecimal(15000));
			samsung.setCategory("SmartPhone");

			Product dg = new Product("Dragon Ball Manga Comic", new BigDecimal(300));
			dg.setCategory("Comic");

			productRepository.save(oppo);
			productRepository.save(samsung);
			productRepository.save(dg); */
		};
	}

}
