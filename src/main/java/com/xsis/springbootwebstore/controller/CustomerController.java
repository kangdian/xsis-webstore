package com.xsis.springbootwebstore.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import com.xsis.springbootwebstore.model.Customer;
import com.xsis.springbootwebstore.repository.CustomerRepository;

@RestController
public class CustomerController {
    @Autowired
    private CustomerRepository customerRepository;

    @RequestMapping("/customers")
    @Transactional
    public List<Customer> getCustomer() {
        return customerRepository.findAll();
    }
    
}