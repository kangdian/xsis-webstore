package com.xsis.springbootwebstore.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.springbootwebstore.model.Product;
import com.xsis.springbootwebstore.services.ProductService;


@RestController
public class ProductController {

    @Autowired
    private ProductService productService;

    @RequestMapping("/products")
    @Transactional
    public Iterable<Product> getProduct() {
        return productService.getAllProduct();
    }


    
}