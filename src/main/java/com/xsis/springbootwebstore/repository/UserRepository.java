package com.xsis.springbootwebstore.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.xsis.springbootwebstore.model.Users;

@Repository
public interface UserRepository extends CrudRepository<Users, Long>{
    Users findByUsername(String username);
    
}