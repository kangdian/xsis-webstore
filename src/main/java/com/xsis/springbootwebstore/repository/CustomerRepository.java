package com.xsis.springbootwebstore.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import com.xsis.springbootwebstore.model.Customer;
import org.springframework.data.jpa.repository.Query;

@RepositoryRestResource
public interface CustomerRepository extends JpaRepository<Customer, Long> {
   
    /*  @Query("select c from customer c where c.address=?1")
    List<Customer> findCustomerByCity(String address); */
    
    @Query("SELECT c FROM Customer c WHERE address= ?1")
    List<Customer> findByCity(String city);
    
}