package com.xsis.springbootwebstore.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.xsis.springbootwebstore.model.Product;

@RepositoryRestResource
public interface ProductRepository extends CrudRepository<Product, Long>{

    // Fetch product by category
    List<Product> findByCategory(@Param("category") String category);

    List<Product> findBymanufacturer(@Param("manufacturer") String manufacturer);

}