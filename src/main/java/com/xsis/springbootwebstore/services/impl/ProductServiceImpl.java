package com.xsis.springbootwebstore.services.impl;

import com.xsis.springbootwebstore.model.Product;
import com.xsis.springbootwebstore.services.ProductService;
import com.xsis.springbootwebstore.repository.ProductRepository;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public Iterable<Product> getAllProduct() {
        return productRepository.findAll();
    }
    
}