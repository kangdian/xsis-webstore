package com.xsis.springbootwebstore.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.xsis.springbootwebstore.repository.UserRepository;
import com.xsis.springbootwebstore.model.Users;

@Service
public class UserDetailServiceImpl implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Users currentUser = userRepository.findByUsername(username);
        System.out.println("current user : "+currentUser);
        UserDetails user = new org.springframework.security.core.userdetails.User(username, currentUser.getPassword(),
                true, true, true, true, AuthorityUtils.createAuthorityList(currentUser.getRole()));
        System.out.println("user : "+user);
        return user;
    }
    
}