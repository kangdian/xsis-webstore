package com.xsis.springbootwebstore.services;

import java.util.List;


import com.xsis.springbootwebstore.model.Product;


public interface ProductService {

    Iterable<Product> getAllProduct();    

}